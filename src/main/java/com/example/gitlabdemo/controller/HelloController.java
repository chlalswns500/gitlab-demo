package com.example.gitlabdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class HelloController {
    @GetMapping("/hello")
    public String hello() {
        return "hello3";
    }
    @GetMapping("/bye")
    public String bye() {
        return "bye";
    }
}
